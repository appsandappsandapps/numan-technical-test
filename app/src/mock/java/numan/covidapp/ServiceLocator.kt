package numan.covidapp

import android.content.Context
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.repositories.CountriesRepositoryInMemory

object ServiceLocator {

  lateinit var countriesRepo: CountriesRepository

  fun init(context: Context) {
    countriesRepo = CountriesRepositoryInMemory()
  }
}
