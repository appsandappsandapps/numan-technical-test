package numan.covidapp.ui.detail

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import numan.covidapp.AppNavigationScreen
import numan.covidapp.MainActivity
import numan.covidapp.R
import numan.covidapp.ServiceLocator
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Running against dummy data -- doesn't touch the internet
 *
 * Values based on the CountriesRepositoryInMemory
 */
@RunWith(AndroidJUnit4::class)
class DetailFragmentTests {

  @get:Rule val composeTestRule = createComposeRule()

  @Test fun checkDetailFragmentIsThere() {
    composeTestRule.setContent {
      ServiceLocator.countriesRepo.deleteBookmarks()
      AppNavigationScreen()
    }
    composeTestRule
      .onNodeWithText("Countries", ignoreCase = true)
      .performClick()
    composeTestRule
      .onNodeWithText("Country A", ignoreCase = true)
      .performClick()
    composeTestRule
      .onNodeWithText("Test country", ignoreCase = true)
      .assertExists()
  }

}