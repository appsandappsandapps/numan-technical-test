package numan.covidapp.ui.bookmarks

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import numan.covidapp.AppNavigationScreen
import numan.covidapp.MainActivity
import numan.covidapp.R
import numan.covidapp.ServiceLocator
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Running against dummy data -- doesn't touch the internet
 *
 * Values based on the RepositoryInMemory
 */
@RunWith(AndroidJUnit4::class)
class BookmarksFragmentTest {

  @get:Rule val composeTestRule = createComposeRule()

  @Test fun checkBookmarksTab() {
    composeTestRule.setContent {
      ServiceLocator.countriesRepo.deleteBookmarks()
      AppNavigationScreen()
    }

    composeTestRule
      .onNodeWithText("Bookmarks", ignoreCase = true)
      .assertExists()

    composeTestRule
      .onNodeWithText("Bookmarks", ignoreCase = true)
      .performClick()

  }

  @Test fun checkBookmarksTabNumWorking() {
    composeTestRule.setContent {
      ServiceLocator.countriesRepo.deleteBookmarks()
      AppNavigationScreen()
    }

    composeTestRule
      .onNodeWithText("1", ignoreCase = true)
      .assertDoesNotExist()

    composeTestRule
      .onNodeWithText("Countries", ignoreCase = true)
      .performClick()

    composeTestRule
      .onNodeWithTag("country checkbox Country A")
      .performClick()

    composeTestRule
      .onNodeWithText("1", ignoreCase = true)
      .assertExists()

    composeTestRule
      .onNodeWithTag("country checkbox Country B")
      .performClick()

    composeTestRule
      .onNodeWithText("2", ignoreCase = true)
      .assertExists()

    composeTestRule
      .onNodeWithTag("country checkbox Country B")
      .performClick()

    composeTestRule
      .onNodeWithText("1", ignoreCase = true)
      .assertExists()

  }

}