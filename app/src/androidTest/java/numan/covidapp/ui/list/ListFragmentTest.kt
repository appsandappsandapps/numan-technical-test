package numan.covidapp.ui.list

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import numan.covidapp.AppNavigationScreen
import numan.covidapp.MainActivity
import numan.covidapp.R
import numan.covidapp.ServiceLocator
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Running against dummy data -- doesn't touch the internet
 *
 * Values based on the RepositoryInMemory
 */
@RunWith(AndroidJUnit4::class)
class ListFragmentTest {

  @get:Rule val composeTestRule = createComposeRule()

  @Test fun checkSearchResults() {
    composeTestRule.setContent {
      ServiceLocator.countriesRepo.deleteBookmarks()
      AppNavigationScreen()
    }
    composeTestRule
      .onNodeWithText("Country A", ignoreCase = true)
      .assertExists()
    composeTestRule
      .onNodeWithText("Country B", ignoreCase = true)
      .assertExists()
    composeTestRule
      .onNodeWithText("Country C", ignoreCase = true)
      .assertExists()
  }

}