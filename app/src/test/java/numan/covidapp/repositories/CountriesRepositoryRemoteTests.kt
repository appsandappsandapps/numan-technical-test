package numan.covidapp.repositories

import numan.covidapp.utils.launchAndWait
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import numan.covidapp.datasources.CountriesDatasource
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class CountriesRepositoryRemoteTests {

  @Before fun setUp() = Dispatchers.setMain(StandardTestDispatcher())
  @After fun tearDown() = Dispatchers.resetMain()

  @Mock lateinit var remote: CountriesDatasource
  @Mock lateinit var bookmarkDao: BookmarksDatasource

  @Test
  fun `repo checks bookmark status for country`() = runTest {
    val id = "0"
    `when`(bookmarkDao.getAll()).thenReturn(listOf())
    val repo = CountriesRepositoryRemote(bookmarkDao, remote)
    launchAndWait { repo.getCountry(id) }

    verify(bookmarkDao, times(1)).get(id)
  }

  @Test
  fun `repo gets bookmarks on init`() = runTest {
    `when`(bookmarkDao.getAll()).thenReturn(listOf())
    launchAndWait {
      CountriesRepositoryRemote(
        bookmarkDao,
        remote,
        Dispatchers.Main,
      )
    }

    verify(bookmarkDao, times(1)).getAll()
  }

}