package numan.covidapp.ui.detail

import androidx.lifecycle.SavedStateHandle
import numan.covidapp.Application
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.utils.StateSaver
import numan.covidapp.utils.launchAndWait
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.test.*
import numan.covidapp.data.Country
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.validateMockitoUsage


@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class DetailViewModelTests {

  @Before fun setUp() = Dispatchers.setMain(StandardTestDispatcher())
  @After fun tearDown() = Dispatchers.resetMain()
  @After fun validate() {
    validateMockitoUsage()
  }
  @Mock lateinit var app: Application
  @Mock lateinit var savedState: SavedStateHandle
  @Mock lateinit var repo: CountriesRepository
  @Mock lateinit var uiState: DetailUIState

  @Test fun `viewmodel calls repo country on init`() = runTest {
    val id = "1"
    `when`(repo.country).thenReturn(MutableStateFlow(Country()))
    launchAndWait {
      DetailViewModel(StateSaver(savedState), id, repo,
        uiState, Dispatchers.Main)
    }

    verify(repo, times(1)).getCountry(id)
  }

  @Test fun `viewmodel calls ui set country on init`() = runTest {
    val country = Country("a")
    `when`(repo.country).thenReturn(MutableStateFlow(country))
    var foundCountry: Country? = null
    launchAndWait {
      val vm = DetailViewModel(StateSaver(savedState), "a", repo,
        null, Dispatchers.Main)
      foundCountry = vm.uiState.stateFlow.take(2).last().country
    }

    Assert.assertEquals(country.name, foundCountry!!.name)
  }

}
