package numan.covidapp.ui.detail

import androidx.lifecycle.SavedStateHandle
import numan.covidapp.Application
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.*
import numan.covidapp.data.Country
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class ArtistDetailUIStateTests {

  @Mock lateinit var app: Application
  @Mock lateinit var savedState: SavedStateHandle
  @Mock lateinit var viewModel: DetailViewModel

  @Test fun `uistate calls viewmodel bookmark`() = runTest {
    val id = "1"
    val uiValues = DetailUIState.UIValues(
      country = Country(id)
    )
    DetailUIState(viewModel, uiValues)
      .update(DetailUIState.Action.Bookmark())

    verify(viewModel, times(1)).bookmark(id, id)
  }

  @Test fun `uistate sets loading on init`() = runTest {
    val uiState = DetailUIState(viewModel)
    val state = uiState.stateFlow.first()

    Assert.assertTrue(state.loading)
  }

}
