package numan.covidapp.ui.list

import androidx.lifecycle.SavedStateHandle
import numan.covidapp.Application
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.*
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class ListUIStateTests {

  @Mock lateinit var app: Application
  @Mock lateinit var savedState: SavedStateHandle
  @Mock lateinit var viewModel: ListViewModel

  @Test fun `uistate calls viewmodel bookmark`() = runTest {
    val artistId = "1"
    val artistName = "2"
    ListUIState(viewModel)
      .update(ListUIState.Action.Bookmark(artistId, artistName))

    verify(viewModel, times(1)).bookmark(artistId, artistName)
  }

  @Test fun `uistate loading true initially`() = runTest {
    val uiState = ListUIState(viewModel)
    val loading = uiState.stateFlow.first().loading

    Assert.assertTrue(loading)
  }

  @Test fun `uistate sets loading on get countries`() = runTest {
    val uiState = ListUIState(viewModel)
    uiState.update(ListUIState.Action.GetCountries())
    val loading = uiState.stateFlow.first().loading

    Assert.assertTrue(loading)
  }

}
