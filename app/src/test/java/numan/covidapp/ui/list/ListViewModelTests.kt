package numan.covidapp.ui.list

import androidx.lifecycle.SavedStateHandle
import numan.covidapp.Application
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.utils.StateSaver
import numan.covidapp.utils.launchAndWait
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.*
import numan.covidapp.data.Bookmarks
import numan.covidapp.data.Countries
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class ListViewModelTests {

  @Before fun setUp() = Dispatchers.setMain(StandardTestDispatcher())
  @After fun tearDown() = Dispatchers.resetMain()

  @Mock lateinit var savedState: SavedStateHandle
  @Mock lateinit var repo: CountriesRepository
  @Mock lateinit var uiState: ListUIState

  @Test fun `viewmodel calls repo artist on init`() = runTest {
    `when`(repo.countries).thenReturn(MutableStateFlow(Countries()))
    `when`(repo.bookmarks).thenReturn(MutableStateFlow(Bookmarks()))
    launchAndWait {
      ListViewModel(StateSaver(savedState), {}, repo,
        uiState, Dispatchers.Main)
    }

    verify(repo, times(1)).countries
  }

  @Test fun `viewmodel calls repo bookmarks on init`() = runTest {
    `when`(repo.countries).thenReturn(MutableStateFlow(Countries()))
    `when`(repo.bookmarks).thenReturn(MutableStateFlow(Bookmarks()))
    launchAndWait {
      ListViewModel(StateSaver(savedState), {}, repo,
        uiState, Dispatchers.Main)
    }

    verify(repo, times(1)).bookmarks
  }

}
