package numan.covidapp.ui.bookmarks

import androidx.lifecycle.SavedStateHandle
import numan.covidapp.Application
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.utils.StateSaver
import numan.covidapp.utils.launchAndWait
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.*
import numan.covidapp.data.Bookmarks
import numan.covidapp.data.Country
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
@ExperimentalCoroutinesApi
class BookmarksViewModelTests {

  @Before fun setUp() = Dispatchers.setMain(StandardTestDispatcher())
  @After fun tearDown() = Dispatchers.resetMain()

  @Mock lateinit var app: Application
  @Mock lateinit var savedState: SavedStateHandle
  @Mock lateinit var repo: CountriesRepository
  @Mock lateinit var uiState: BookmarksUIState

  @Test fun `viewmodel calls repo bookmarks on init`() = runTest {
    `when`(repo.bookmarks).thenReturn(MutableStateFlow(Bookmarks()))
    launchAndWait {
      BookmarksViewModel(StateSaver(savedState), {}, repo,
        uiState, Dispatchers.Main)
    }

    verify(repo, times(1)).bookmarks
  }

  @Test fun `viewmodel calls repo debookmark`() = runTest {
    val artistId = "1"
    `when`(repo.country).thenReturn(MutableStateFlow(Country()))
    launchAndWait {
      val vm = BookmarksViewModel(StateSaver(savedState), {}, repo,
        uiState, Dispatchers.Main)
      vm.debookmark(artistId)
    }

    verify(repo, times(1)).debookmark(artistId)
  }

}
