package numan.covidapp

import android.content.Context
import numan.covidapp.datasources.CountriesDatasourceREST
import numan.covidapp.datasources.GenericRESTSource
import numan.covidapp.datasources.createBookmarksDatastore
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.repositories.CountriesRepositoryRemote
import numan.covidapp.utils.BooksmarksDatabaseFactory

/**
 * In lieu of Dagger / Hilt -- since there are only Application level dependencies
 */

object ServiceLocator {

  lateinit var countriesRepo: CountriesRepository

  fun init(context: Context) {
    val db = createBookmarksDatastore(createDatabase(context))
    val countriesDatasource = CountriesDatasourceREST(GenericRESTSource())
    countriesRepo = CountriesRepositoryRemote(
      db,
      countriesDatasource,
    )
  }

  private fun createDatabase(context: Context) =
    BooksmarksDatabaseFactory(context).createDatabase()

}