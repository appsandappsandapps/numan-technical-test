package numan.covidapp.repositories

import androidx.annotation.VisibleForTesting
import kotlinx.coroutines.flow.StateFlow
import numan.covidapp.data.*

interface CountriesRepository {
  val countries: StateFlow<Countries>
  val country: StateFlow<Country>
  val bookmarks: StateFlow<Bookmarks>
  fun deleteBookmarks() // For testing
  suspend fun getAllCountries()
  suspend fun getCountry(id: String)
  suspend fun bookmark(id: String, name: String): Unit
  suspend fun debookmark(id: String): Unit
}

