package numan.covidapp.repositories

import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import numan.covidapp.data.*
import java.util.*

class CountriesRepositoryInMemory : CountriesRepository {

  override val bookmarks = MutableStateFlow(Bookmarks(listOf()))
  override val country: StateFlow<Country> get() = MutableStateFlow(
    Country(
      name = "Test country",
      All = CountryData(
        country = "Test country name",
        population = 1000000,
      )
    )
  )
  override val countries = MutableStateFlow(Countries(
    countries = listOf(
      Country(name = "Country A"),
      Country(name = "Country B"),
      Country(name = "Country C"),
    )
  ))

  override fun deleteBookmarks() {
    bookmarks.value = Bookmarks(listOf())
  }

  override suspend fun getAllCountries() {}

  override suspend fun bookmark(id: String, name: String) {
    bookmarks.value = Bookmarks(
      bookmarks.value.bookmarks.toMutableList().apply {
        add(Bookmark(id, name))
      }
    )
  }

  override suspend fun debookmark(id: String) {
    var bms = bookmarks.value.bookmarks.filter { it.id != id }
    bookmarks.value = Bookmarks(bms)
  }

  override suspend fun getCountry(id: String) {
  }

}
