package numan.covidapp.repositories

import numan.covidapp.data.*
import numan.covidapp.utils.IODispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import numan.covidapp.datasources.CountriesDatasource

interface BookmarksDatasource {
  suspend fun getAll(): List<Bookmark>
  suspend fun get(id: String): Bookmark?
  suspend fun delete(id: String)
  suspend fun insert(id: String, name: String)
}

/**
 * Merges a bookmarks database
 * with the rest endpoint
 */
class CountriesRepositoryRemote(
  private val bookmarksDatasouce: BookmarksDatasource,
  private val countriesDatastore: CountriesDatasource,
  private val dispatcher: CoroutineDispatcher = IODispatcher
): CountriesRepository {

  override val country = MutableStateFlow(Country())
  override val bookmarks = MutableStateFlow(Bookmarks(listOf()))
  override val countries = MutableStateFlow(Countries())

  init {
    MainScope().launch(dispatcher) {
      refreshBookmarks()
    }
  }

  override suspend fun getAllCountries() {
    try {
      val countriesList = countriesDatastore
        .getAll()
        .toList()
      countries.value = Countries(countries = countriesList)
    } catch(e: Exception) {
      countries.value = Countries(error = ""+e)
    }
  }

  override suspend fun getCountry(id: String) {
    try {
      val foundCountry = countriesDatastore.get(id)
      val bookmarked = bookmarksDatasouce.get(id)
      if(bookmarked != null) {
        country.value = foundCountry.copy(bookmarked = true)
      } else {
        country.value = foundCountry
      }
    } catch(e: Exception) {
      country.value = Country(error = ""+e)
    }
  }

  override suspend fun bookmark(id: String, name: String) {
    try {
      bookmarksDatasouce.insert(id, name)
      refreshBookmarks()
    } catch (e: Exception) {
      // TODO: Generic error stateflow
      // ATM, we're unsure what page this could
      // be called from
    }
  }

  override suspend fun debookmark(id: String) {
    try {
      bookmarksDatasouce.delete(id)
      refreshBookmarks()
    } catch(e: Exception) {
      // TODO: Generic error stateflow
      // ATM, we're unsure what page this could
      // be called from
    }
  }

  private suspend fun isBookmarked(id: String) =
    bookmarksDatasouce.get(id) != null

  private suspend fun refreshBookmarks() {
    val bookmarksEntities = bookmarksDatasouce.getAll()
    bookmarks.value = Bookmarks(
      bookmarksEntities.map { Bookmark(it.id, it.name) }
    )
  }

  // Used only for testing the mock branch
  override fun deleteBookmarks() {}

}