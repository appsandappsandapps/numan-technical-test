package numan.covidapp.datasources

import numan.covidapp.BookmarksDatabase
import numan.covidapp.data.Bookmark
import numan.covidapp.repositories.BookmarksDatasource

fun createBookmarksDatastore(db: BookmarksDatabase): BookmarksDatasource {
  val dao = db.bookmarksQueries
  return object : BookmarksDatasource {
    override suspend fun getAll(): List<Bookmark> =
      dao.getAll().executeAsList().map { Bookmark(it.id, it.name) }

    override suspend fun get(id: String): Bookmark? =
      dao.get(id).executeAsOneOrNull()?.let { Bookmark(it.id, it.name) }

    override suspend fun delete(id: String) =
      dao.delete(id)

    override suspend fun insert(id: String, name: String) =
      dao.insert(id, name)
  }
}
