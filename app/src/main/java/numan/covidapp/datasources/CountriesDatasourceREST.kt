package numan.covidapp.datasources

import numan.covidapp.data.Country
import numan.covidapp.data.CountryData

class CountriesDatasourceREST(
  val genericRESTService: GenericRESTSource,
) : CountriesDatasource {

  override suspend fun getAll(): List<Country> {
    val url = "https://covid-api.mmediagroup.fr/v1/vaccines"
    val response = genericRESTService
      .get<Map<String, Country>>(url)
      .map {
        it.value.copy(name = it.key)
      }
    return response
  }

  override suspend fun get(countryName: String): Country {
    val url = "https://covid-api.mmediagroup.fr/v1/vaccines?country=${countryName}"
    val response = genericRESTService.get<Country>(url)
    return response.copy(name = countryName)
  }

}
