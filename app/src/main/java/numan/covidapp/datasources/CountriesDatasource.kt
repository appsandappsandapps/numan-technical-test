package numan.covidapp.datasources

import numan.covidapp.data.Country

interface CountriesDatasource {
  suspend fun getAll(): List<Country>
  suspend fun get(countryName: String): Country
}