package numan.covidapp.data

data class Bookmarks (
  val bookmarks: List<Bookmark> = listOf(),
)