package numan.covidapp.data

import kotlinx.serialization.Serializable
import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize
import java.util.*

@Parcelize
@Serializable
data class Country(
  val name: String = "",
  val error: String = "",
  val bookmarked: Boolean = false,
  val All: CountryData = CountryData(),
  val date: Long = Date().time,
): Parcelable