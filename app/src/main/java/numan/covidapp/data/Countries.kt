package numan.covidapp.data

import kotlinx.serialization.Serializable
import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize
import java.util.*

@Parcelize
@Serializable
data class Countries(
  val error: String = "",
  val countries: List<Country> = listOf(),
  val date: Long = Date().time
) : Parcelable