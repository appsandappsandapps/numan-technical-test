package numan.covidapp.data

import kotlinx.serialization.Serializable
import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize

@Parcelize
@Serializable
data class CountryData(
  val continent: String = "",
  val country: String = "",
  val administered: Long = 0,
  val population : Long = 0,
  val people_vaccinated: Long = 0,
  val people_partially_vaccinated: Long = 0,
  val updated: String? = null,
) : Parcelable
