package numan.covidapp.data

import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize

@Parcelize data class Bookmark (
  val id: String = "",
  val name: String = "",
) : Parcelable