package numan.covidapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import numan.covidapp.ui.theme.NumanCovidTheme


class MainActivity : ComponentActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent { NumanCovidTheme { AppNavigationScreen() } }
  }

}
