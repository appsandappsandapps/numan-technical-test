package numan.covidapp

import android.app.Application

class Application : Application() {

  override fun onCreate() {
    super.onCreate()
    // Depends on the build flavour's ServiceLocator
    ServiceLocator.init(this)
  }

}