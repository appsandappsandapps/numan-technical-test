package numan.covidapp.utils

import android.os.Parcelable
import kotlinx.coroutines.Dispatchers
import kotlinx.parcelize.Parcelize

/**
 * These all differ in a KMM project
 */

typealias Parcelable = Parcelable
typealias Parcelize = Parcelize
// Okay okay, this is a value
val IODispatcher = Dispatchers.IO

