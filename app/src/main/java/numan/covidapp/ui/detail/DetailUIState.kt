package numan.covidapp.ui.detail

import numan.covidapp.data.Country
import numan.covidapp.ui.detail.DetailUIState.Action.*
import numan.covidapp.ui.detail.DetailUIState.UIValues
import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize
import numan.covidapp.utils.UIState

class DetailUIState(
  private val viewModel: DetailViewModel,
  private var existing: UIValues = UIValues(),
  private val saveToParcel: (UIValues) -> Unit = {},
) : UIState<UIValues>(existing, saveToParcel) {

  @Parcelize data class UIValues(
    val country: Country = Country(),
    var loading: Boolean = true,
    var error: String = "",
  ): Parcelable

  sealed class Action {
    class Bookmark: Action()
    class Debookmark: Action()
    class SetBookmarked(val bookmarked: Boolean): Action()
    class SetCountry(val country: Country): Action()
    class ServerError(val error: String): Action()
  }

  fun update(action: Action): Any = when(action) {
    is SetBookmarked -> {
      val country = stateData.country.copy(bookmarked = action.bookmarked)
      stateData = stateData.copy(country = country)
    }
    is Bookmark -> {
      viewModel.bookmark(stateData.country.name, stateData.country.name)
    }
    is Debookmark -> {
      viewModel.debookmark()
    }
    is SetCountry -> {
      stateData = stateData.copy(
        country = action.country,
        loading = false,
        error = "",
      )
    }
    is ServerError -> {
      stateData = stateData.copy(error = action.error, loading = false)
    }
  }

}
