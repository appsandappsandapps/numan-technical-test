package numan.covidapp.ui.detail

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import numan.covidapp.ServiceLocator
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.ui.detail.DetailUIState.Action.*
import numan.covidapp.utils.*

class DetailViewModel(
  savedState: StateSavable,
  private val countryId: String,
  private val repository: CountriesRepository = ServiceLocator.countriesRepo,
  private var mockUiState: DetailUIState? = null,
  dispatcher: CoroutineDispatcher = IODispatcher,
): DispatchedViewModel(dispatcher) {

  constructor(
    artistId: String,
  ) : this(StateSaverEmpty(), artistId)

  val uiState = mockUiState ?:
    DetailUIState(
      viewModel = this,
      existing = savedState.get(DetailUIState.UIValues()),
      saveToParcel = { savedState.save(it) }
    )

  init {
    observeCountry()
    observeBookmarks()
  }

  private fun observeCountry() = dispatchedLaunch {
    repository.getCountry(countryId)
    repository.country.collect {
      if(it.error.isNotBlank()) {
        uiState.update(ServerError(it.error))
      } else {
        uiState.update(SetCountry(it))
      }
    }
  }

  private fun observeBookmarks() = dispatchedLaunch {
    repository.bookmarks.collect {
      val currentArtist =
        it.bookmarks.filter { it.id == countryId }
      if(currentArtist.size == 1) {
        uiState.update(SetBookmarked(true))
      } else {
        uiState.update(SetBookmarked(false))
      }
    }
  }

  public fun bookmark(id: String, name: String) = dispatchedLaunch {
    repository.bookmark(id, name)
  }

  public fun debookmark() = dispatchedLaunch {
    repository.debookmark(countryId)
  }

}
