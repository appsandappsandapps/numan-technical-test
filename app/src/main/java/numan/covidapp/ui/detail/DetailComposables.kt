package numan.covidapp.ui.detail

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import numan.covidapp.utils.StateSaver
import numan.covidapp.utils.ext.viewModelWithSavedState
import numan.covidapp.ui.detail.DetailUIState.Action.*
import numan.covidapp.ui.theme.StandardPadding
import numan.covidapp.utils.ext.findActivity

/**
 * - Shows country info
 * - Bookmark
 * - Unbookmark
 * - Show loading
 * - Show error
 */
@Composable
fun DetailScreen(
  countryName: String,
) {
  val activity = LocalContext.current.findActivity()
  val stateObj = viewModelWithSavedState {
    DetailViewModel(
      StateSaver(it),
      countryName,
    )
  }.uiState
  val values = stateObj.stateFlow.collectAsState().value

  Surface {
    DetailContent(
      values.country.name,
      values.country.All.administered,
      values.country.All.people_vaccinated,
      values.country.All.people_partially_vaccinated,
      values.country.All.population,
      values.country.bookmarked,
      values.country.All.updated,
      values.loading,
      values.error,
      { stateObj.update(Bookmark()) },
      { stateObj.update(Debookmark()) },
    )
  }
}

@Composable
private fun DetailContent(
  name: String,
  administered: Long,
  vaccinated: Long,
  partiallyVaccinated: Long,
  population: Long,
  bookmarked: Boolean,
  updated: String?,
  loading: Boolean,
  error: String,
  bookmark: () -> Unit,
  debookmark: () -> Unit,
) {
  val padding = StandardPadding
  val context = LocalContext.current
  if(loading)
    LinearProgressIndicator(
      Modifier.fillMaxWidth(),
    )
  else if(error.isBlank())
    Column(
      Modifier
        .padding(10.dp)
        .verticalScroll(rememberScrollState())
    ) {
      Row(Modifier.padding(padding)) {
        Checkbox(
          bookmarked,
          { if (it) bookmark() else debookmark() },
        )
        Text(
          name,
          style = MaterialTheme.typography.h4,
        )
      }
      DetailDivier()
      Text(
        "Population: ${"%,d".format(population)}",
        style = MaterialTheme.typography.body1,
      )
      DetailDivier()
      Text(
        "Administered: ${"%,d".format(administered)}",
        style = MaterialTheme.typography.body1,
      )
      DetailDivier()
      Text(
        "Vaccinated: ${"%,d".format(vaccinated)}",
        style = MaterialTheme.typography.body1,
      )
      DetailDivier()
      Text(
        "Partially vaccinated: ${"%,d".format(partiallyVaccinated)}",
        style = MaterialTheme.typography.body1,
      )
      DetailDivier()
      val date = updated ?: "No information"
      Text(
        "Updated: ${date}",
        style = MaterialTheme.typography.body1,
      )
    }
  else
    Toast.makeText(context, error, Toast.LENGTH_LONG).show()
}

@Composable
private fun DetailDivier() {
  Divider(
    Modifier.padding(0.dp, 10.dp),
    color = MaterialTheme.colors.onBackground
  )
}
