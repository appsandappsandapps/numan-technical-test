package numan.covidapp.ui.homepage

import numan.covidapp.ui.homepage.HomepageUIState.Action.*
import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize
import numan.covidapp.utils.UIState
import numan.covidapp.ui.homepage.HomepageUIState.UIValues

class HomepageUIState(
  private val viewModel: HomepageViewModel,
  private val existing: UIValues = UIValues(),
  private val saveToParcel: (UIValues) -> Unit = {},
) : UIState<UIValues>(existing, saveToParcel) {

  @Parcelize data class UIValues(
    val bookmarked: Int = 0,
  ): Parcelable

  sealed class Action {
    class Bookmarks(val num: Int): Action()
  }

  fun update(action: Action): Any = when(action) {
    is Bookmarks -> {
      stateData = stateData.copy(bookmarked = action.num)
    }
  }

}
