package numan.covidapp.ui.homepage

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collect
import numan.covidapp.ServiceLocator
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.ui.homepage.HomepageUIState.Action.*
import numan.covidapp.utils.*

class HomepageViewModel(
  private val savedState: StateSavable,
  private val repository: CountriesRepository = ServiceLocator.countriesRepo,
  private var mockUiState: HomepageUIState ? = null,
  dispatcher: CoroutineDispatcher = IODispatcher,
): DispatchedViewModel(dispatcher) {

  constructor() : this(StateSaverEmpty())

  val uiState = mockUiState ?:
    HomepageUIState(
      viewModel = this,
      existing = savedState.get(HomepageUIState.UIValues()),
      saveToParcel = { savedState.save(it) }
    )

  init {
    observeArtists()
  }

  private fun observeArtists() = dispatchedLaunch {
    repository.bookmarks.collect {
      uiState.update(Bookmarks(it.bookmarks.size))
    }
  }

}
