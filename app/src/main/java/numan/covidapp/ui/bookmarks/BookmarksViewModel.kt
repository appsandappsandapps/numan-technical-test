package numan.covidapp.ui.bookmarks

import numan.covidapp.ServiceLocator
import numan.covidapp.repositories.CountriesRepository
import numan.covidapp.utils.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collect

class BookmarksViewModel(
  private val savedState: StateSavable,
  public var gotoDetail: (String) -> Unit = {},
  private val repository: CountriesRepository = ServiceLocator.countriesRepo,
  private var mockUiState: BookmarksUIState? = null,
  dispatcher: CoroutineDispatcher = IODispatcher,
): DispatchedViewModel(dispatcher) {

  constructor(
    gotoDetail: (String) -> Unit = {}
  ) : this(StateSaverEmpty(), gotoDetail)

  val uiState = mockUiState ?:
    BookmarksUIState(
      viewModel = this,
      existing = savedState.get(BookmarksUIState.UIValues()),
      saveToParcel = { savedState.save(it) }
    )

  init {
    observeArtists()
  }

  private fun observeArtists() = dispatchedLaunch {
    repository.bookmarks.collect {
      uiState.update(BookmarksUIState.Action.SetBookmarks(it.bookmarks))
    }
  }

  public fun debookmark(id: String) = dispatchedLaunch {
    repository.debookmark(id)
  }

  public fun gotoDetailScreen(id: String) {
    gotoDetail(id)
  }

}
