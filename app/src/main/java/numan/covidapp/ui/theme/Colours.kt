package numan.covidapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val DarkestGreen = Color(0xFF13bf5c)
val Teal200 = Color(0xFF03DAC5)
val OffWhite = Color(0xFFE2E2E2)
val OffWhiteSurface = Color(0xFFF8F8F8)
