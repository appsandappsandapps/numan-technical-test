package numan.covidapp.ui.list

import numan.covidapp.ServiceLocator
import numan.covidapp.repositories.CountriesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collect
import numan.covidapp.ui.list.ListUIState.Action.*
import numan.covidapp.utils.*

class ListViewModel(
  private val savedState: StateSavable,
  public var gotoDetail: (String) -> Unit = {},
  private val repository: CountriesRepository = ServiceLocator.countriesRepo,
  private var mockUiState: ListUIState ? = null,
  dispatcher: CoroutineDispatcher = IODispatcher,
): DispatchedViewModel(dispatcher) {

  constructor(
    gotoDetail: (String) -> Unit,
  ) : this(StateSaverEmpty(), gotoDetail)

  val uiState = mockUiState ?:
    ListUIState(
      viewModel = this,
      existing = savedState.get(ListUIState.UIValues()),
      saveToParcel = { savedState.save(it) }
    )

  init {
    dispatchedLaunch { observeCountries() }
    dispatchedLaunch { observeBookmarks() }
  }

  private suspend fun observeCountries() {
    repository.getAllCountries()
    repository.countries.collect {
      if(it.error.isNotBlank()) {
        uiState.update(ServerError(it.error))
      } else {
        uiState.update(AddCountries(it))
      }
    }
  }

  private suspend fun observeBookmarks() {
    repository.bookmarks.collect {
      val ids = it.bookmarks.map { it.id }
      uiState.update(BookmarksMerge(ids))
    }
  }

  public fun bookmark(id: String, name: String) = dispatchedLaunch {
    repository.bookmark(id, name)
  }

  public fun getCountries() = dispatchedLaunch {
    repository.getAllCountries()
  }

  public fun debookmark(id: String) = dispatchedLaunch {
    repository.debookmark(id)
  }

  public fun gotoArtistDetail(id: String) {
    gotoDetail(id)
  }

}
