package numan.covidapp.ui.list

import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.input.ImeAction
import numan.covidapp.NavControllerLocal
import numan.covidapp.utils.StateSaver
import numan.covidapp.utils.ext.viewModelWithSavedState
import numan.covidapp.ui.list.ListUIState.Action.*
import numan.covidapp.ui.theme.StandardPadding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import numan.covidapp.data.Countries

/**
 * - Shows loading
 * - Shows error
 * - Can clear text input
 * - Click to goto country detail
 * - Click to bookmark
 * - Unclick to bookmark
 */
@Composable
fun SearchListScreen() {
  val controller = NavControllerLocal.current
  val gotoDetail: (String) -> Unit = {
    controller.navigate("detail/"+it)
  }
  // View Model & UI State
  val viewModel = viewModelWithSavedState {
    ListViewModel(
      StateSaver(it),
      gotoDetail,
    )
  }
  viewModel.gotoDetail = gotoDetail // else crash -- keeping onto old fragment
  val stateObj = viewModel.uiState
  val uiValues = stateObj.stateFlow.collectAsState().value

  SearchListContent(
    SearchResultsWithState(stateObj, uiValues),
    uiValues.loading,
    uiValues.error,
    clearError = { stateObj.update(ServerError("")) },
  )

}

/**
 * Shows the list
 * and any errors and if there's no results
 * and loading
 */
@Composable
private fun SearchListContent(
  resultsSlot: @Composable () -> Unit,
  loading: Boolean,
  error: String,
  clearError: () -> Unit,
) {
  val scope = rememberCoroutineScope()
  val context = LocalContext.current
  Box(
    Modifier.fillMaxSize()
  ) {
    resultsSlot()
    if(loading) {
      CircularProgressIndicator(
        Modifier.align(Alignment.Center)
      )
    }
  }
  if(error.isNotBlank()) {
    Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    scope.launch {
      delay(400)
      clearError()
    }
  }
}

@Composable
private fun SearchResultsWithState(
  stateObj: ListUIState,
  uiValues: ListUIState.UIValues,
): @Composable () -> Unit = {
  SearchResults(
    uiValues.countries,
    ArtistRowWithState(stateObj),
    { stateObj.update(ListUIState.Action.GetCountries()) },
    uiValues.showRetry,
  )
}

@Composable
private fun SearchResults(
  countries: Countries,
  artistRowSlot: @Composable (id: String, name: String, boolean: Boolean) -> Unit,
  retry: () -> Unit,
  showRetry: Boolean,
) {
  Box(
    Modifier.fillMaxSize()
  ) {
    if(showRetry) {
      Button(
        { retry() },
        Modifier.align(Alignment.Center),
      ) {
        Text("Retry?")
      }
    }
    LazyColumn {
      items(countries.countries) { country ->
        key(country.All.country, country.bookmarked) {
          artistRowSlot(country.name, country.name, country.bookmarked)
        }
      }
    }
  }
}

private fun ArtistRowWithState(
  stateObj: ListUIState
): @Composable (String, String, Boolean) -> Unit =
  { id, name, bookmarked ->
    CountryRow(
      id,
      name,
      bookmarked,
      { id, name -> stateObj.update(Bookmark(id, name)) },
      { stateObj.update(Debookmark(it)) },
      { stateObj.update(GotoArtistDetail(id)) },
    )
  }

/**
 * Includes ability to (un)bookmark
 */
@Composable
private fun CountryRow(
  id: String,
  name: String,
  bookmarked: Boolean,
  bookmark: (String, String) -> Unit,
  debookmark: (String) -> Unit,
  gotodetail: (String) -> Unit,
) {
  Row(
    Modifier.fillMaxWidth(),
    verticalAlignment = Alignment.CenterVertically,
  ) {
    CountryBookmarker(
      id,
      name,
      bookmarked,
      bookmark,
      debookmark,
      Modifier.testTag("country checkbox ${id}")
    )
    Text(
      name,
      Modifier
        .clickable { gotodetail(id) }
        .weight(1F)
        .fillMaxHeight()
    )
  }
}

@Composable
private fun CountryBookmarker(
  id: String,
  name: String,
  bookmarked: Boolean,
  bookmark: (String, String) -> Unit,
  debookmark: (String) -> Unit,
  modifier: Modifier = Modifier,
) {
  var checked by remember { mutableStateOf(bookmarked) }
  Checkbox(
    checked,
    {
      checked = it
      if (it) bookmark(id, name) else debookmark(id)
    },
    modifier,
  )
}