package numan.covidapp.ui.list

import numan.covidapp.data.Countries
import numan.covidapp.ui.list.ListUIState.Action.*
import numan.covidapp.ui.list.ListUIState.UIValues
import numan.covidapp.utils.Parcelable
import numan.covidapp.utils.Parcelize
import numan.covidapp.utils.UIState

class ListUIState(
  private val viewModel: ListViewModel,
  private var existing: UIValues = UIValues(),
  private val saveToParcel: (UIValues) -> Unit = {},
) : UIState<UIValues>(existing, saveToParcel){

  @Parcelize data class UIValues(
    val loading: Boolean = true,
    val error: String = "",
    val countries: Countries = Countries(),

  ): Parcelable {
    val showRetry = !loading && countries.countries.isEmpty()
  }

  sealed class Action {
    class GetCountries(): Action()
    class Bookmark(val id: String, val name: String): Action()
    class Debookmark(val id: String) : Action()
    class GotoArtistDetail(val id: String) : Action()
    class AddCountries(val countries: Countries) : Action()
    class ServerError(val error: String) : Action()
    // When we (un)bookmark, update the items on the screen
    class BookmarksMerge(val ids: List<String>) : Action()
  }

  fun update(action: Action): Any = when(action) {
    is GetCountries -> {
      stateData = stateData.copy(loading = true)
      viewModel.getCountries()
    }
    is Bookmark -> {
      viewModel.bookmark(action.id, action.name)
    }
    is Debookmark -> {
      viewModel.debookmark(action.id)
    }
    is GotoArtistDetail -> {
      viewModel.gotoArtistDetail(action.id)
    }
    is AddCountries -> {
      stateData = stateData.copy(
        error = "",
        loading = false,
        countries = action.countries,
      )
    }
    is ServerError -> {
      stateData = stateData.copy(
        loading = false,
        error = action.error
      )
    }
    is BookmarksMerge -> {
      applyBookmarksToItms(action.ids)
    }
  }

  private fun applyBookmarksToItms(bookmarkIds: List<String>) {
    val countries = stateData.countries
    val newCountries = countries.countries.map {
      if(bookmarkIds.contains(it.All.country)) {
        it.copy(bookmarked = true)
      } else {
        it.copy(bookmarked = false)
      }
    }
    stateData = stateData.copy(countries = Countries(countries = newCountries))
  }

}
